module UCC_MSDFF_always(input clk,rst,cin,input [1:0] m,input [7:0] pin,
                        output reg cout,output reg [7:0] fout);
	always @(posedge clk, posedge rst) begin
		if(rst==1) fout <= 8'b0;
		else begin
			case(m)
				0: fout = fout;
				1: {cout,fout} = fout + 1;
				2: {cout,fout} = fout - 1;
				3: fout = pin;
				default: fout <= fout;
			endcase
		end
	end
endmodule