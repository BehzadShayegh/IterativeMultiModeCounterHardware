module HIC_TB();
  reg [7:0] fin = 8;
	reg [7:0] pin = 4;
	reg cin;
	reg [1:0] m;
	reg clk = 0;
	
	wire cout;
	wire [7:0] fout = 8;
	
	HIC hic(pin,cin,clk,m,cout,fout);
		          	
	integer i;
	always #300 clk = ~clk;
	initial begin
	#300 m=3; cin = 1;
	  for(i=0; i<=3; i=i+1) begin
	     #1000 m=i;
              cin = 1;
	        #600 cin = 0;
	        #600 cin = 1;
         	#600 cin = 0;
   	end
	end
endmodule

