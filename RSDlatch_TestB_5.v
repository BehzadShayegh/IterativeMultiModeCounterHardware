module RSDlatch_TB();
	reg clk = 0;
	reg D = 0;
	wire o;
	RSDlatch Dlatch(D,clk,o);
	
	always #300 clk = ~clk;
	initial begin
		#800 D = 1;
		#800 D = 0;
		#800 D = 1;
		#800 D = 0;
	end
endmodule