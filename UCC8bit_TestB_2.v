module UCC8bit_TB();
	reg [7:0] fin = 7;
	reg [7:0] pin = 4;
	reg cin;
	reg [1:0] m;
	
	wire cout; 
	wire [7:0] fout;
	
	UCC8bit u(fin,pin,cin,m,cout,fout);
	
	integer i;
	initial begin
	  for(i=0; i<=3; i=i+1) begin
	     #400 m=i;
              cin = 1;
	        #400 cin = 0;
	        #400 cin = 1;
         	#400 cin = 0;
   	end
	end
endmodule