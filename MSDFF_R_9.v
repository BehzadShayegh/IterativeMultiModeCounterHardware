module MSDFF_R(input D,clk,rst,output o);
  wire D_;
   
  and #(7) g(D_,D,~rst);
  MSDFF msdff(D_,clk,o);
  
endmodule