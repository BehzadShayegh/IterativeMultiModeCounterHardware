module UCC_TB();
	integer i;
	reg [1:0] m = 2'b00;
	reg cin=0,fin=0,pin=0;
	wire fout,cout;
	UCC ucc(m,cin,fin,pin,fout,cout);
	initial begin
		for(i=0;i<=3;i=i+1) begin
			#500 m = i;
				#400 cin = 0;
					#300 fin = 0;
						#200 pin = 0;
						#200 pin = 1;
					#300 fin = 1;
						#200 pin = 0;
						#200 pin = 1;
				#400 cin = 1;
					#300 fin = 0;
						#200 pin = 0;
						#200 pin = 1;
					#300 fin = 1;
						#200 pin = 0;
						#200 pin = 1;
		end
	end
endmodule
