module SRlatch_TB ();
	reg clk=1;
	reg set=0;reg rst=0;
	wire o;
	RSlatch SR(clk,set,rst,o);
	always #100 clk = ~clk;
	initial begin
		#200 rst = 1;
		#200 rst = 0;
		#200 set = 1;
		#200 set = 0;
		#200 rst = 1;
		#200 set = 1;
		#200 rst = 0;
		#200 set = 0;
	end
endmodule