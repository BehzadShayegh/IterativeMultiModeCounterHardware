module RSlatch(input clk,set,reset,output o);
  wire o_;
	wire after1,after2;
	nand #(7) gate1(after1,clk,set);
	nand #(7) gate2(after2,clk,reset);
	nand #(7) gate3(o,after1,o_);
	nand #(7) gate4(o_,after2,o);
endmodule
