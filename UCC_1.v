module UCC(input [1:0] m,input cin,fin,pin,output fout,cout);
	
	wire after2,after3,after4,after5,after6,after7,after8,after9,after10,after11,after12;
	
	and #(7) gate12(after12,m[1],~m[0]),
	         gate11(after11,~m[1],m[0]);
	and #(7) gate10(after10,after11,cin,fin),
	         gate9(after9,after12,cin,~fin);
	or  #(7) gate13(cout,after9,after10);
	xor #(7) gate8(after8,fin,cin);
	and #(7) gate7(after7,pin,m[1],m[0]),
	         gate6(after6,after8,after12),
	         gate5(after5,after8,after11),
	         gate4(after4,fin,~m[1],~m[0]);
	or  #(7) gate3(after3,after6,after7),
	         gate2(after2,after5,after4),
	         gate1(fout,after2,after3);
endmodule