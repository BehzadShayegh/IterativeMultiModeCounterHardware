module UCC_MSDFF_assign_TB();
	reg cin = 1;
	reg rst = 0;
	reg clk = 0;
	reg [1:0] m = 3;
	reg [7:0] pin = 8'b00111010;
	wire cout;
	wire [7:0] fout;
	
	UCC_MSDFF_assign um(clk,rst,cin,m,pin,cout,fout);
	
	always #200 clk = ~clk;
	initial begin
	  #800 rst = 1;
		repeat(5) begin
			#1000 pin = $random;
			       rst = 0;
		#400 m=1;
		#600 cin = 1;
		#800 m = 2;
		#400 pin = 8'b00110100;
		#800 m=3;
		#1600 cin = 0;
		#400 m=0;
		end
		#800 rst = 1;
	end
endmodule