module MSDFF_R_TB();
	reg clk = 0;
	reg D = 0;
	reg rst = 0;
	wire o;
	MSDFF_R msd_ff(D,clk,rst,o);
	
	always #300 clk = ~clk;
	initial begin
		#800 D = 1;
		#800 D = 0;
		#800 D = 1;
		#800 rst = 1;
		#800 D = 0;
		#800 rst = 0;
	end
endmodule