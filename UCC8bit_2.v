module UCC8bit(input [7:0] fin,pin, input cin, input [1:0] m,
		          	output cout,	output [7:0] fout);
	wire [8:0] w;
	genvar i;
	generate
		for(i = 0; i<=7; i=i+1) begin
			UCC ucc(m,w[i],fin[i],pin[i],fout[i],w[i+1]);
		end
	endgenerate
	
	assign w[0] = cin;
	assign cout = w[8];
endmodule