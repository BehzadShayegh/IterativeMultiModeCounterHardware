module MSDFF(input D,clk,output o);
	wire o_;
	RSDlatch D1(D,clk,o_);
	RSDlatch D2(o_,~clk,o);
endmodule