module UCC_MSDFF_assign(input clk,rst,cin,input [1:0] m,input [7:0] pin,
                        output cout, inout[7:0] fout);
	reg [7:0] f;
	reg c;
	always @(posedge clk, posedge rst) begin
		if(rst==1) f = 8'b0;
		else begin
	    f = fout;
			case(m)
			  0: f = f;
				1: {c,f} = f + 1;
				2: {c,f} = f - 1;
				3: f = pin;
				default: f = f;
			endcase
		end
	end
	assign fout = f;
	assign cout = c;
endmodule