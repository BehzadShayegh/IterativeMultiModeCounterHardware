module UCC_MSDFF(input clk,rst,cin,input [1:0] m,input [7:0] pin,
                 output cout,output[7:0] fout);
	wire [7:0] fin;
	genvar i;
	generate
		for(i = 0; i<=7; i = i+1)begin
			MSDFF_R msdff_r(fout[i],clk,rst,fin[i]);
		end
	endgenerate
	UCC8bit u(fin,pin,cin,m,cout,fout);
endmodule 