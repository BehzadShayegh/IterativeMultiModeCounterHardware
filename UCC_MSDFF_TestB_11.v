module UCC_MSDFF_TB();
	reg cin = 0;
	reg rst = 0;
	reg clk = 0;
	
	reg [1:0] m;
	reg [7:0] pin = 8'b0;
	wire [7:0] fout;
	wire cout;
	
	UCC_MSDFF ucc_msdff(clk,rst,cin,m,pin,cout,fout);
	
	always #200 clk = ~clk;
	
	initial begin
		m[1] = 0;
		m[0] = 0;
		#300 m = 3;
		#500 cin = 1;
		#200 m = 2;
		#500 pin = 7'b0100011;
		#200 m = 1;
		#1000 cin = 0;
		#400 m = 0;
		#1000 m = 3;
		#500 rst = 1;
		#200 m = 0;
	end	
endmodule 